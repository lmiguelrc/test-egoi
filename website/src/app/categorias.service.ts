import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

let headers = new HttpHeaders({
  'Content-Type': 'application/json',
});
let options = {
    headers: headers
}

@Injectable({
  providedIn: 'root'
})

export class CategoriasService {

  constructor(private http: HttpClient) { }

  getCategorias() {
      return this.http.get('http://localhost:8080/api/categoria')
  }

  getCategoriaById(categoria_id) {
      return this.http.get('http://localhost:8080/api/categoria/'+categoria_id);
  }

  addCategoria(novaCategoria) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    let options = {
        headers: headers
    }
    return this.http.post('http://localhost:8080/api/categoria', novaCategoria, options);
  }
  
  editCategoria(updatedCategoria){
    console.log(updatedCategoria);
    return this.http.get('http://localhost:8080/api/categoria/'+updatedCategoria.id);
  }

  deleteCategoria(categoria_id) {
    return this.http.delete('http://localhost:8080/api/categoria/' + categoria_id);
  }
}
