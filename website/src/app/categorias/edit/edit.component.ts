import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CategoriasService } from '../../categorias.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  editForm: FormGroup;
  dadosCategoria: Object;
  constructor(private formBuilder: FormBuilder, private router: Router, private categorias: CategoriasService) { }

  ngOnInit() {
    let categoriaId = window.localStorage.getItem("editCatId");
    if(!categoriaId) {
      alert("Invalid action.")
      this.router.navigate(['categorias/list']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      nome: ['', Validators.required]
    });
    /*this.categorias.getCategoriaById(categoriaId).subscribe( categorias => {
        this.editForm.setValue(categorias[0].nome);
    });*/

    this.categorias.getCategoriaById(categoriaId).subscribe( categorias => {
      this.dadosCategoria = categorias;
    });

  }

  onSubmit() {
    this.categorias.editCategoria(this.editForm.value).subscribe( categorias => {
      this.router.navigate(['categorias/list']);
    });
  }

  voltar() {
    this.router.navigate(['categorias/list']);
  }

}
