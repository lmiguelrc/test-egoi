import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { CategoriasService } from '../../categorias.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private categorias: CategoriasService) { }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      nome: ['', Validators.required]
    });
  }

  onSubmit() {
    this.categorias.addCategoria(this.addForm.value).subscribe( data => {
        this.router.navigate(['categorias/list']);
    });
  }

  voltar() {
    this.router.navigate(['categorias/list']);
  }


}
