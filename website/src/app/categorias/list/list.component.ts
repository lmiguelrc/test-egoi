import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { CategoriasService } from '../../categorias.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  dadosCategorias: Object;
  
  constructor(private router: Router, private categorias: CategoriasService) { }

  ngOnInit() {
    this.categorias.getCategorias().subscribe(categorias => {
        this.dadosCategorias = categorias;
      }
    )
  }

  addCategoria() {
    this.router.navigate(['categorias/add']);
  }

  editCategoria(categoria) {
    window.localStorage.removeItem("editCatId");
    window.localStorage.setItem("editCatId", categoria.id.toString());
    this.router.navigate(['categorias/edit']);
  }

  deleteCategoria(categoria) {
    this.categorias.deleteCategoria(categoria.id).subscribe(categorias => {
        this.dadosCategorias = categorias;
      }
    )
  }
}
