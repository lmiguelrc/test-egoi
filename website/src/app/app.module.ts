import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { SobreComponent } from './sobre/sobre.component';
import { ContactosComponent } from './contactos/contactos.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ListComponent } from './categorias/list/list.component';
import { AddComponent } from './categorias/add/add.component';
import { EditComponent } from './categorias/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SobreComponent,
    ContactosComponent,
    HomepageComponent,
    ListComponent,
    AddComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
