import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent} from './homepage/homepage.component';
import { SobreComponent} from './sobre/sobre.component';
import { ContactosComponent} from './contactos/contactos.component';
import { ListComponent} from './categorias/list/list.component';
import { AddComponent} from './categorias/add/add.component';
import { EditComponent} from './categorias/edit/edit.component';

const routes: Routes = [
  {path: '', component: HomepageComponent },
  {path: 'categorias/list', component: ListComponent },
  {path: 'sobre', component: SobreComponent },
  {path: 'contactos', component: ContactosComponent },
  {path: 'categorias/add', component: AddComponent },
  {path: 'categorias/edit', component: EditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
