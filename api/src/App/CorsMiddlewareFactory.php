<?php

namespace App\Cors;

use Tuupola\Middleware\Cors;
use Zend\Diactoros\Response;
use Zend\Stratigility\Middleware\CallableMiddlewareWrapper;

class CorsMiddlewareFactory
{
    public function __invoke($container)
    {
        //$corsConfig = $container->get('config')['cors'] ?? [];
        //return new CorsMiddleware($corsConfig);

        return new CallableMiddlewareWrapper(
            new Cors([
                "origin" => ["*"],
                "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
                "headers.allow" => ["Content-Type", "Accept"],
                "headers.expose" => [],
                "credentials" => false,
                "cache" => 0,
            ]),
            new Response()
        );
    }
}