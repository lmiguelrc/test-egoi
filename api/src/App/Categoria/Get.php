<?php

declare(strict_types=1);

namespace App\Categoria;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class Get implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    private $tableGateway;

    public function __construct(string $containerName, Router\RouterInterface $router, ?TemplateRendererInterface $template = null,  $tableGateway = null) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
        $this->tableGateway = $tableGateway;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    { 
        $id_categoria = $request->getAttribute('id_categoria');
        $results = $this->tableGateway->select(['id' => $id_categoria])->toArray();
        $status = 200;

        if(count($results) == 0) {
            $status = 404;
        }

        return new JsonResponse($results, $status);
        
    }
}
