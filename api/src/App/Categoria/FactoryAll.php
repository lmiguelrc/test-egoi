<?php

declare(strict_types=1);

namespace App\Categoria;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;


use function get_class;

class FactoryAll
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;
        $config = $container->get('config');
        $adapter = new Adapter($config['db']);
        $tableGateway = new TableGateway('categorias', $adapter);

        return new GetAll(get_class($container), $router, $template, $tableGateway);
    }
}
